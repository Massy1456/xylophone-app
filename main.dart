import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  const XylophoneApp({Key? key}) : super(key: key);

  static AudioCache player = AudioCache();

  void playSound(int soundNum) {
    final player = AudioCache();
    player.play('assets_note${soundNum}.wav');
  }

  Expanded buildKey({required int num, required Color color}) {
    return Expanded(
      child: Container(
        color: color,
        child: TextButton(
          onPressed: () {
            playSound(num);
          },
          child: Text(''),
        ),
      ),
    ); // TextButton
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildKey(num: 1, color: Colors.red),
                buildKey(num: 2, color: Colors.orange),
                buildKey(num: 3, color: Colors.yellow),
                buildKey(num: 4, color: Colors.green),
                buildKey(num: 5, color: Colors.blue),
                buildKey(num: 6, color: Colors.deepPurple),
                buildKey(num: 7, color: Colors.purple),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

